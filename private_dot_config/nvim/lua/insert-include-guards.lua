local M = {}

function M.setup()
  local function get_uuid_program()
    if vim.fn.executable("uuidgen") == 1 then
      return "uuidgen"
    end
    if vim.fn.executable("uuid") == 1 then
      return "uuid"
    end
    return ""
  end

  local function generate_define_value()
    local uuid_program = get_uuid_program()
    if uuid_program == "" then
      error("Could not find a program to generate uuids. Looked for `uuid` and `uuidgen`")
      return
    end
    local uuid = vim.fn.system(uuid_program)
    uuid = uuid:upper()
    uuid = uuid:gsub("-", "_")
    uuid = uuid:sub(1, -2)
    uuid = "UUID_" .. uuid
    return uuid
  end

  local function insert_include_guard()
    local uuid = generate_define_value()
    vim.fn.append(0, "#ifndef " .. uuid)
    vim.fn.append(1, "#define " .. uuid)
    vim.fn.append(vim.fn.line("$"), "#endif")
  end

  vim.api.nvim_create_user_command("InsertIncludeGuard", insert_include_guard, { desc = "Adds C++ include guards" })
end

return M
