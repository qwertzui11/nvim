-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- remap `<Leader>ft` to use fish
-- https://github.com/LazyVim/LazyVim/blob/66b3c0a19fe3024d4bf1da563f7ca5a9aff58dec/lua/lazyvim/config/keymaps.lua#L143
local Util = require("lazyvim.util")
local shell = nil
if vim.fn.executable("fish") == 1 then
  shell = "fish"
end
local lazyterm = function()
  Snacks.terminal(shell, { cwd = Util.root() })
end
local map = Util.safe_keymap_set
map("n", "<leader>ft", lazyterm, { desc = "Terminal (root dir)" })
map("n", "<leader>fT", function()
  Snacks.terminal(shell)
end, { desc = "Terminal (cwd)" })
map("n", "<c-/>", lazyterm, { desc = "Terminal (root dir)" })
map("n", "<c-_>", lazyterm, { desc = "which_key_ignore" })

if vim.fn.executable("qalc") == 1 then
  map("n", "<leader>k", function()
    Snacks.terminal("qalc")
  end, { desc = "Open the calculate `qalc`" })
end
