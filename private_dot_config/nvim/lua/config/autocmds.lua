-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua

local function augroup(name)
  return vim.api.nvim_create_augroup(name, { clear = true })
end
-- close some more filetypes with <q>
vim.api.nvim_create_autocmd("FileType", {
  group = augroup("close_with_q"),
  pattern = {
    "OverseerList",
  },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
    vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
  end,
})

-- fix comment in C++ from `/* %s */` to `// %s`
vim.api.nvim_create_autocmd("FileType", {
  pattern = { "cpp" },
  desc = "cpp commentstring configuration",
  -- command = "setlocal commentstring=//\\ %s",
  callback = function()
    vim.bo.commentstring = "// %s"
  end,
})
