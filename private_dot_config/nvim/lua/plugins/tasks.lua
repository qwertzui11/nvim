return {
  {
    -- https://github.com/stevearc/overseer.nvim
    -- launches the `preLaunch` task.
    "stevearc/overseer.nvim",
    config = true,
    lazy = true,
    cmd = { "OverseerRun", "OverseerToggle", "OverseerRun", "OverseerLoadBundle", "OverseerSaveBundle" },
    -- stylua: ignore
    keys = {
      { "<leader>rs", "<cmd>OverseerToggle<cr>", desc = "Toggle the overseer window Summary" },

      { "<leader>rr", "<cmd>OverseerRun<cr>", desc = "Run a task from a template" },
      { "<leader>ra", "<cmd>OverseerQuickAction<cr>", desc = "Run an action on the most recent task, or the task under the cursor", },

      { "<leader>rl", "<cmd>OverseerLoadBundle<cr>", desc = "Load tasks that were saved to disk" },
      { "<leader>rw", "<cmd>OverseerSaveBundle<cr>", desc = "Serialize and save the current tasks to disk" },
      { "<leader>rd", "<cmd>OverseerDeleteBundle<cr>", desc = "Delete a saved task bundle" },
    },
  },
  {
    "nvim-neotest/neotest",
    opts = function(_, opts)
      local neotest_overseer = require("neotest.consumers.overseer")
      opts.consumers = { overseer = neotest_overseer }
    end,
  },
  {
    "nvim-lualine/lualine.nvim",
    opts = function(_, opts)
      local overseer = require("overseer")
      table.insert(opts.sections.lualine_x, {
        "overseer",
        symbols = {
          [overseer.STATUS.RUNNING] = "󰜎 ",
        },
      })
    end,
  },
}
