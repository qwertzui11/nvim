return {
  {
    "rcarriga/nvim-dap-ui",
    enabled = false,
  },
  {
    "jay-babu/mason-nvim-dap.nvim",
    opts = {
      handlers = {
        codelldb = function(config)
          -- https://github.com/jay-babu/mason-nvim-dap.nvim#advanced-customization
          require("mason-nvim-dap").default_setup(config)
        end,
      },
    },
  },
}
