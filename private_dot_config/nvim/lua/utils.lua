local M = {}

function M.is_arch_amd64()
  local os_uname = vim.uv.os_uname()
  return os_uname.machine == "x86_64"
end

return M
