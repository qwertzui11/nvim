# Dotfiles

This repository contains all dotfiles configurations, like `.config/nvim` or `.config/fish`.

## Installation

To install the dotfiles [Ansible](https://docs.ansible.com/ansible/latest/index.html) is needed.

```bash
sudo apt update && sudo apt install -y python3-pip 
python3 -m pip install --user ansible
```

To set up everything call on your local machine run

```bash
ansible-playbook \
  --ask-become-pass \
  --inventory inventory/localhost.ini \
  fish.yaml \
  rust.yaml \
  neovim.yaml \
  neovim-config.yaml
```

This will also install tools like `git`, `fd` and `rg`.

When setting up a remote server also install `kitty-terminfo.yaml` and `ssh.yaml`.

```bash
ansible-playbook \
  --ask-become-pass \
  --inventory inventory/remote.yaml \
  update-system.yaml \
  fish.yaml \
  rust.yaml \
  neovim.yaml \
  neovim-config.yaml \
  kitty-terminfo.yaml \
  ssh.yaml
```

Add `--ask-become-pass` if the remote user needs a password for `sudo`, or [set `ansible_password`](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) (not recommended!).

An example `inventory/remote.yaml` looks like

```yaml
all:
  hosts:
    # change `fun-host` to a host declared in your [](~/.ssh/config)
    # else set `ansible_port: 22`, `ansible_host: ip-address` and `ansible_user`
    fun-host: 
      ansible_python_interpreter: /usr/bin/python3
```

## tips

### clipboard

Install [`wl-clipboard-rs`](https://github.com/YaLTeR/wl-clipboard-rs)
or [`wl-clipboard`](https://github.com/bugaevc/wl-clipboard) for `Wayland`
clipboard support.

Install `xsel` for `X11` clipboard support.

### Set working directory to the current file

- [fandom](https://vim.fandom.com/wiki/Set_working_directory_to_the_current_file)
- command: `:cd %:p:h`
  - `:p` gives fill path of file (`%`)
  - `:h` gives `head` of path

### Human readable `ansible` logs

Add `~/.ansible.cfg` with the content

```yaml
[defaults]
stdout_callback = yaml
```

## dependencies

- [`fzf`](https://github.com/junegunn/fzf#installation)

## font installation

```bash
ansible-playbook font.yaml
```
